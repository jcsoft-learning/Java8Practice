package com.lycsoftware.concurrency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Logger;

/**
 * Similar a RecursiveAction pero en este caso se devuelve un resultado en el computo que luego será juntado (join)
 * en un resultado final.
 */
public class CustomRecursiveTask extends RecursiveTask<Integer>
{
    private static final Logger logger = Logger.getAnonymousLogger();
    private static final int THRESHOLD = 20;
    private final int[] arr;

    public CustomRecursiveTask(final int[] arr)
    {
        this.arr = arr;
    }

    @Override
    protected Integer compute()
    {
        if (arr.length > THRESHOLD) {
            return ForkJoinTask.invokeAll(createSubtasks())
                    .stream()
                    .mapToInt(ForkJoinTask::join)
                    .sum();
        } else {
            return processing(arr);
        }
    }

    private Collection<CustomRecursiveTask> createSubtasks()
    {
        List<CustomRecursiveTask> dividedTasks = new ArrayList<>();
        dividedTasks.add(new CustomRecursiveTask(
                Arrays.copyOfRange(arr, 0, arr.length / 2)));
        dividedTasks.add(new CustomRecursiveTask(
                Arrays.copyOfRange(arr, arr.length / 2, arr.length)));
        return dividedTasks;
    }

    private Integer processing(final int[] arr)
    {
        logger.info("Array: " + arr);
        return Arrays.stream(arr)
                .filter(a -> a > 10 && a < 27)
                .map(a -> a * 10)
                .sum();
    }
}
