package com.lycsoftware.collections;

import java.util.*;
import java.util.function.Consumer;

public class ApiImprovements
{
    private static Consumer<Object> consumerTest = s -> System.out.println(s);
    private static Consumer<Object> consumerTest2 = null;
    private static Consumer<Object> consumerTest2() {
        return s -> System.out.println(s);
    }

    public static void main(String[] args)
    {
        //Arrays.stream(new String[]{"1","2","3"});
        // hace una evaluacion del consumer pasado al forEach para que no sea null
        Arrays.asList("Jorge", "Lucia", null, "Nico")
                .iterator().forEachRemaining(consumerTest);

        Map<String, String> map = new HashMap<>();
        map.put("1", "uno");
        map.put("2", "dos");
        map.put("3", "cuatro");
        // permite realizar alguna evaluación sobre el value del key a insertar por si ya existe, además retorna el
        // nuevo valor si existe sino devuelve nulo
        map.compute("3", (key, value) -> !"tres".equals(value) ? "tres".concat("JOJO") : value);
        map.compute("4", (key, value) -> value == null ? "cuatro" : value.toUpperCase());
        // permite realizar alguna acción entre el nuevo value y el antiguo si este existe.
        map.merge("4", "nuevoCuatro", (val1, val2) -> val1.concat(val2));
        map.merge("5", "nuevoCinco", (val1, val2) -> val1.concat(val2));
        map.forEach((k, v) -> System.out.println("key: " + k + ", value: " + v));


    }


}
