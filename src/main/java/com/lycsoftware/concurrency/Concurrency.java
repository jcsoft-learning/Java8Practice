package com.lycsoftware.concurrency;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Concurrency
{
    public static void main(final String[] args)
            throws ExecutionException, InterruptedException
    {
        final Concurrency con = new Concurrency();
        con.completableFuture();
        con.forkJoin();
    }

    /**
     * Lo mejor de CompletableFuture es que se pueden combinar stages, es decir que se pueden encadenar pasos de un
     * proceso para que se vayan ejecutando.
     * Java 8 introdujo la clase CompletableFuture. Junto con la interfaz Future, también implementó la interfaz
     * CompletionStage. Esta interfaz define el contrato para un paso de cálculo asincrónico que podemos combinar con
     * otros pasos.
     * CompletableFuture es al mismo tiempo un bloque de construcción y un framework, con aproximadamente 50 métodos
     * diferentes para componer, combinar y ejecutar pasos de cálculo asincrónico y manejar errores.
     */
    private void completableFuture()
            throws ExecutionException, InterruptedException
    {
        final CompletableFuture<Integer> prov1 = CompletableFuture.supplyAsync(() -> 5);
        final CompletableFuture<Integer> prov2 = CompletableFuture.supplyAsync(() -> 10);

        final Integer sum = prov1.thenCombine(prov2, Integer::sum).get();
        System.out.println(sum);

        // thenCompose es similar a Optional.flatMap o Stream.flatMap. Utiliza el valor del CompletionStage inicial
        // en el CompletionStage pasado como función, devolviendo un CompletionStage derivado de ambos.
        final CompletableFuture<Integer> prov3 = CompletableFuture.supplyAsync(() -> 15);
        final Integer sum2 = prov3.thenCompose(integer -> CompletableFuture.supplyAsync(() -> 12 + integer)).get();
        System.out.println(sum2);

        // thenApply es similar a Optional.map o Stream.map, ya que utiliza el valor del CompletionStage inicial
        // (prov4) y lo modifica en la función recibida, devolviendo como resultado el mismo tipo del
        // CompletableFuture (Integer en este caso)
        final CompletableFuture<Integer> prov4 = CompletableFuture.supplyAsync(() -> 12);
        final Integer sum3 = prov4.thenApply(integer -> integer + 10).get();
        System.out.println(sum3);

        // thenAccept a diferencia que thenApply recibe un Consumer no un Function por lo que devolverá un
        // CompletableFunction Void.
        final CompletableFuture<Integer> prov5 = CompletableFuture.supplyAsync(() -> 25);
        final CompletableFuture<Void> a = prov5.thenAccept(integer -> System.out.println(integer));

        // CompletableFuture.join permite devolver el valor una vez completo y a diferencia de get lanza los
        // unchecked exceptions pero no las propaga.
        final CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> "Hello");
        final CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> "World");
        final CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> "!!!");

        final String combined = Stream.of(future1, future2, future3)
                .map(CompletableFuture::join)
                .collect(Collectors.joining(" "));
        System.out.println(combined);

        // aqui la unica diferencia aparente es que usamos thenApplyAsync, pero internamente hay más, la aplicación
        // de la función (apply) está envuelta (wrapped) en una instancia de ForkJoinTask, lo que permitirá una mejor
        // paralelización.
        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> "Hello");
        CompletableFuture<String> future = completableFuture.thenApplyAsync(s -> s + " World");
        System.out.println(future.get());
    }

    /**
     * ForkJoin se presentó en Java 7. Proporciona herramientas para ayudar a acelerar el procesamiento paralelo al
     * intentar utilizar todos los núcleos de procesador disponibles, lo que se logra mediante un enfoque de divide
     * y vencerás.
     * En la práctica, esto significa que el marco primero se "bifurca", dividiendo recursivamente la tarea en
     * subtareas independientes más pequeñas hasta que sean lo suficientemente simples como para ejecutarse de forma
     * asincrónica.
     * Después de eso, comienza la parte de "join", en la que los resultados de todas las subtareas se unen
     * recursivamente en un solo resultado, o en el caso de una tarea que devuelve vacío, el programa simplemente
     * espera hasta que se ejecutan todas las subtareas.
     * Para proporcionar una ejecución paralela efectiva, el el framework ForkJoin utiliza un grupo de
     * subprocesos llamado ForkJoinPool, que administra subprocesos de trabajo de tipo ForkJoinWorkerThread.
     * <p>
     * ForkJoinPool es el corazón del framework y se encarga de manejar hilos de trabajo (Workers Thread) proveyendo
     * información del estado y rendimiento del thread pool.
     * Los Worker Threads solo pueden ejecutar una tarea al mismo tiempo pero ForkJoinPool no crea un thread para
     * cada subtarea sino que los reutiliza de acuerdo como vayan terminando. Por default un Worker Thread
     * coge su tarea siguiente del head de su propio deque (double-ended queue: cola que puede agregar elementos
     * desde el head o back), pero si no tiene más tareas va y coge una tarea de la cola de otro thread, evitando que
     * los threads compitan por tareas.
     */
    private void forkJoin()
    {
        ForkJoinPool commonPool = ForkJoinPool.commonPool();
        final CustomRecursiveTask customRecursiveTask = new CustomRecursiveTask(new int[] {10, 21, 33, 4, 51, 64, 16,
                21, 45, 61, 23, 87, 96, 34, 22, 1, 3, 57, 98, 25, 36, 44, 68, 78});
        commonPool.execute(customRecursiveTask);
        int result = customRecursiveTask.join();
        System.out.println(result);
    }
}
