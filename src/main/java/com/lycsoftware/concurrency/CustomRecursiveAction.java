package com.lycsoftware.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.logging.Logger;

/**
 * Este patrón se puede utilizar para desarrollar sus propias clases de RecursiveAction. Para hacer esto, cree un
 * objeto que represente la cantidad total de trabajo, elija un umbral adecuado, defina un método para dividir el
 * trabajo y defina un método para realizar el trabajo.
 */
public class CustomRecursiveAction extends RecursiveAction
{

    private static final int THRESHOLD = 4;
    private static final Logger logger = Logger.getAnonymousLogger();
    private String workload = "";

    public CustomRecursiveAction(final String workload)
    {
        this.workload = workload;
    }

    @Override
    protected void compute()
    {
        if (workload.length() > THRESHOLD) {
            ForkJoinTask.invokeAll(createSubtasks());
        } else {
            processing(workload);
        }
    }

    private List<CustomRecursiveAction> createSubtasks()
    {
        List<CustomRecursiveAction> subtasks = new ArrayList<>();

        String partOne = workload.substring(0, workload.length() / 2);
        String partTwo = workload.substring(workload.length() / 2);

        subtasks.add(new CustomRecursiveAction(partOne));
        subtasks.add(new CustomRecursiveAction(partTwo));

        return subtasks;
    }

    private void processing(final String work)
    {
        String result = work.toUpperCase();
        logger.info("This result - (" + result + ") - was processed by "
                    + Thread.currentThread().getName());
    }
}
